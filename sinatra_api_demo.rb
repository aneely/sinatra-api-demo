require 'sinatra'
require 'json'
# for pretty debugging output
require 'pp'

# Struct stub for contact class
Contact = Struct.new(:first_name, :last_name, :phone, :email)

helpers do
  # validate login credentials
  def valid_login?(user, password)
    user == 'user' && password == 'password'
  end

  # validate token header
  def valid_token_header?(request)
    request_token = request.env['HTTP_TOKEN']
    valid_token? (request_token)
  end

  def valid_token?(submitted_token)
    submitted_token == current_valid_token
  end

  # generate and expire authentication token
  def current_valid_token
    unless File.exists?('token.tmp')
      File.open('token.tmp', 'w') {|f| f.write(generate_token) }
    end
    File.read('token.tmp')
  end

  def expire_current_token
    File.delete('token.tmp') if File.exist?('token.tmp')
  end

  # JSON payload for a successful login
  def json_response_to_login
    response_hash = {}
    response_hash[:token] = "#{current_valid_token}"
    response_hash[:accountNumber] = '0123456789'
    response_hash[:accountName] = 'ACME COMPANY'

    JSON.pretty_generate(response_hash)
  end

  # JSON payload for /contacts
  def json_response_sample_list_of_size (this_many)
    response_hash = {contactList: []}

    this_many.times do
      contact = generate_contact
      response_hash[:contactList] << {firstName: contact.first_name, lastName: contact.last_name, phone: contact.phone, email: contact.email}
    end

    JSON.pretty_generate(response_hash)
  end

  # random token generator
  def generate_token (length=10)
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
    (0...length).map{ o[rand(o.length)] }.join
  end

  # random contact generator
  def generate_contact
    first_name = %w(Andy Bryan Chris Dawson Ella Fred Gayle Harriet Jake Kyle).sample
    last_name = %w(Lawson Meeks Nelson O'Brien Park Quincy Ryan Smith Tyler Unger).sample
    phone = "#{(0...3).map{rand(1..9)}.join}-#{(0...3).map{rand(1..9)}.join}-#{(0...4).map{rand(0..9)}.join}"
    email = "#{first_name}.#{last_name}@#{2.times.map{%w(inno strat dev comm blue info).sample}.join}.com".downcase

    Contact.new(first_name, last_name, phone, email)
  end
end

# route methods for defined API requests

# e.g. curl -v localhost:4567/login -d 'user=user' -d 'password=password'
post '/login' do
  error 401 unless valid_login?(params[:user], params[:password])

  content_type :json
  json_response_to_login
end

# e.g. curl -v localhost:4567/contacts -H 'token:sOmeT0ken'
get '/contacts' do
  error 401 unless valid_token_header?(request)

  content_type :json
  json_response_sample_list_of_size(rand(1..12))
end

# e.g. curl -v localhost:4567/logout -H 'token:sOmeT0ken'
get '/logout' do
  error 401 unless valid_token_header?(request)

  expire_current_token
  status 200
end

# route methods for undefined API requests
not_found do
  error 400
end

error 401 do
  "{'error':'unauthorized', 'message':'You are not authorized to access this resource.'}"
end
