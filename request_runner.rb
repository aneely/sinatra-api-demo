require 'curb'
require 'json'
require 'slop'
require 'pry' # uncomment to enable pry debugging

opts = Slop.parse(help:true) do
  on 'v', 'verbose', 'Enable verbose mode.'
  on 's', 'step',    'Step through the requests with user prompting.'
end

# for running directly with Sinatra; restarting between changes to the Sinatra code
domain = 'http://localhost:4567'
# for live-reloading Sinatra with Shotgun unless loaded with the -p 4567 argument
# domain = 'http://localhost:9393'
VERBOSE = opts.verbose?
STEP    = opts.step?
DIVIDER = '+--------------------------------------------------------------'

def prompt_if_step(step=STEP)
  if step
    puts "\nPress ENTER to continue #{DIVIDER}"
    gets
  end
end

# illustrating a multipart form post to login

puts "\nRequesting #{domain}/login with 'user=user' and 'password=password'"
puts "\nFirst as a multipart form request\n"

post_domain = "#{domain}/login"
post_user = Curl::PostField.content('user', 'user')
post_password = Curl::PostField.content('password', 'password')

http = Curl::Easy.http_post(post_domain, post_user, post_password) do |curl|
  # remove the next line to pass the arguments as URL encoded instead
  curl.multipart_form_post = true
  curl.verbose = VERBOSE
end

puts "\nResponse status: #{http.status}\n"

token = JSON.parse(http.body_str)['token']

puts "\nReceived the token:#{token}"

prompt_if_step

puts "\nSecond as a URL encoded request\n"

# illustrating a URL encoded with simplified syntax
http = Curl.post(post_domain, {user: 'user', password: 'password'}) do |curl|
  curl.verbose = VERBOSE
end

puts "\nResponse status: #{http.status}\n"

# demonstrating that the current valid token is still the same
puts "\nToken #{token} still valid? #{token == JSON.parse(http.body_str)['token']}\n"

prompt_if_step

puts "\nRequesting #{domain}/contacts with the token #{token}\n"

http = Curl::Easy.perform("#{domain}/contacts") do |curl|
  curl.headers['token'] = token
  curl.verbose = VERBOSE
end

puts "\nResponse status: #{http.status}\n"
puts "\n#{http.body_str}"

prompt_if_step

puts "\nRequesting #{domain}/logout  with the token #{token}\n"

http = Curl::Easy.perform("#{domain}/logout") do |curl|
  curl.headers['token'] = token
  curl.verbose = VERBOSE
end

puts "\nResponse status: #{http.status}\n"

prompt_if_step

puts "\nRequesting #{domain}/contacts  with the expired token #{token}\n"

http = Curl::Easy.perform("#{domain}/contacts") do |curl|
  curl.headers['token'] = token
  curl.verbose = VERBOSE
end

puts "\nResponse status: #{http.status}"
puts "\n#{http.body_str}" # output body

prompt_if_step

puts "\nMaking a request to the root of the domain #{domain} which will not be found\n"

http = Curl::Easy.new("#{domain}/")
http.verbose = VERBOSE
http.perform

puts "\nResponse status: #{http.status}"

puts "\n\nFinished all requests #{DIVIDER}--\n\n"